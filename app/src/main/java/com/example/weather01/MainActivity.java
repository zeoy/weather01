package com.example.weather01;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.WindowManager;

import com.example.weather01.backen.BasicInfor.GetLatAndLng;
import com.example.weather01.ui.datatype.HourlyWeather;
import com.example.weather01.ui.datatype.SevenWeather;
import com.example.weather01.ui.main.SectionsPagerAdapter;
import com.example.weather01.ui.main.WeatherFragment;
import com.example.weather01.ui.main.WeatherInfo;
import com.google.android.material.tabs.TabLayout;
import org.json.JSONException;
import java.util.ArrayList;
import java.util.List;

/**
 * 首页为定位城市
 *
 */

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        @SuppressLint({"NewApi", "LocalSuppress"})
        StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent intent=new Intent(MainActivity.this, GetLatAndLng.class);
        startActivity(intent);



    }
}