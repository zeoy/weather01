package com.example.weather01.ui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weather01.R;
import com.example.weather01.ui.datatype.HourlyWeather;

import java.util.List;

/**
 * 逐小时预报数据列表适配器
 *
 */

public class ShowHourlyAdapter extends RecyclerView.Adapter<ShowHourlyAdapter.ViewHolder> {

    private List<HourlyWeather> list;
    Context context;


    public ShowHourlyAdapter(Context context, List<HourlyWeather> list){
        this.list=list;
        this.context=context;
    }


    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView HourlyTime;
        TextView HourlyTem;
        ImageView HourylState;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            HourlyTime=itemView.findViewById(R.id.hourly_time);
            HourlyTem=itemView.findViewById(R.id.hourly_temperature);
            HourylState=itemView.findViewById(R.id.hourly_weather_state);
        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView=View.inflate(parent.getContext(),R.layout.recycle_weather_hourly,null);
        ViewHolder holder=new ViewHolder(itemView);
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        HourlyWeather hourlyWeather=list.get(position);
        holder.HourlyTime.setText(hourlyWeather.getHourlyTime());
        holder.HourlyTem.setText(hourlyWeather.getHourlyTem());
        holder.HourylState.setBackgroundResource(hourlyWeather.getHourlyIcon());

    }


    @Override
    public int getItemCount() {
        return list!=null?list.size():0;
    }
}
