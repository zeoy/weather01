package com.example.weather01.ui.datatype;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 逐小时预报数据列表
 * Parcelable序列化该类，方便后续使用RecycleView传参
 */

public class HourlyWeather implements Parcelable{

    private String HourlyTime;
    private String HourlyTem;
    private int HourlyIcon;

    public HourlyWeather(String HourlyTime,String HourlyTem,int HourlyIcon){
        this.HourlyTime=HourlyTime;
        this.HourlyTem=HourlyTem;
        this.HourlyIcon=HourlyIcon;
    }

    protected HourlyWeather(Parcel in) {
        HourlyTime = in.readString();
        HourlyTem = in.readString();
        HourlyIcon = in.readInt();
    }

    public String getHourlyTime(){
        return HourlyTime;
    }

    public String getHourlyTem(){
        return HourlyTem;
    }

    public int getHourlyIcon(){
        return HourlyIcon;
    }


    public static final Creator<HourlyWeather> CREATOR = new Creator<HourlyWeather>() {
        @Override
        public HourlyWeather createFromParcel(Parcel in) {
            return new HourlyWeather(in);
        }

        @Override
        public HourlyWeather[] newArray(int size) {
            return new HourlyWeather[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(HourlyTime);
        dest.writeString(HourlyTem);
        dest.writeInt(HourlyIcon);
    }
}
