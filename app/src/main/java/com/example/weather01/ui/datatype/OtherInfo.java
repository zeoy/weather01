package com.example.weather01.ui.datatype;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * 其他天气信息列表
 *
 */
public class OtherInfo implements Parcelable {
    private String sunOut;
//    private String sunDown;
//    private String rainPercent;
//    private String wind;
//    private String windSpeed;
//    private String temBody;
//    private String precipitation;
//    private String airPressure;
//    private String visibility;
//    private String UVIntensity;

    public OtherInfo(String sunOut){
        this.sunOut=sunOut;
    }

    protected OtherInfo(Parcel in) {
        sunOut = in.readString();
    }

    public static final Creator<OtherInfo> CREATOR = new Creator<OtherInfo>() {
        @Override
        public OtherInfo createFromParcel(Parcel in) {
            return new OtherInfo(in);
        }

        @Override
        public OtherInfo[] newArray(int size) {
            return new OtherInfo[size];
        }
    };

    public String getSunOut(){
        return sunOut;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sunOut);
    }
}
