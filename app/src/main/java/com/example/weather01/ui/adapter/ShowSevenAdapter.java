package com.example.weather01.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.weather01.R;
import com.example.weather01.ui.datatype.SevenWeather;

import java.util.List;

/**
 * 未来N天预报数据列表适配器
 */

public class ShowSevenAdapter extends RecyclerView.Adapter<ShowSevenAdapter.ViewHolder> {

    private List<SevenWeather> list;
    Context context;

    public ShowSevenAdapter(Context context, List<SevenWeather> list){
        this.list=list;
        this.context=context;
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView Date;
        TextView TemHeight;
        TextView TemLowest;
        ImageView DayIcon;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            Date=itemView.findViewById(R.id.SevenDate);
            TemHeight=itemView.findViewById(R.id.SevenTemHeight);
            TemLowest=itemView.findViewById(R.id.SevenTemLowest);
            DayIcon=itemView.findViewById(R.id.SevenWeatherState);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView=View.inflate(parent.getContext(),R.layout.recycle_weather_seven,null);
        ShowSevenAdapter.ViewHolder holder=new ShowSevenAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ShowSevenAdapter.ViewHolder holder, int position) {
        SevenWeather sevenWeather=list.get(position);
        holder.Date.setText(sevenWeather.getDate());
        holder.TemHeight.setText(sevenWeather.getTemHeight());
        holder.TemLowest.setText(sevenWeather.getTemLowest());
        holder.DayIcon.setBackgroundResource(sevenWeather.getDayIcon());
    }

    @Override
    public int getItemCount() {
        return list!=null?list.size():0;
    }
}
