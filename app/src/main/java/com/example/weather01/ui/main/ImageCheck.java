package com.example.weather01.ui.main;
import com.example.weather01.R;

import org.jetbrains.annotations.NotNull;

/**
 * 根据API返回值匹配图标
 *
 * BG
 * 字符串匹配 例如只要有雨就放指定图
 */

public class ImageCheck {
    public int imageCheck(@NotNull String text){
        int icon=R.mipmap.i100;
        switch (text){
            case "100":
                icon= R.mipmap.i100;
                break;
            case "101":
                icon= R.mipmap.i101;
                break;
            case "102":
                icon= R.mipmap.i102;
                break;
            case "103":
                icon= R.mipmap.i103;
                break;
            case "104":
                icon= R.mipmap.i104;
                break;
            case "150":
                icon= R.mipmap.i150;
                break;
            case "153":
                icon= R.mipmap.i153;
                break;
            case "154":
                icon= R.mipmap.i154;
                break;
            case "300":
                icon= R.mipmap.i300;
                break;
            case "301":
                icon= R.mipmap.i301;
                break;
            case "302":
                icon= R.mipmap.i302;
                break;
            case "3013":
                icon= R.mipmap.i303;
                break;
            case "304":
                icon= R.mipmap.i304;
                break;
            case "305":
                icon= R.mipmap.i305;
                break;
            case "306":
                icon= R.mipmap.i306;
                break;
            case "307":
                icon= R.mipmap.i307;
                break;
            case "308":
                icon= R.mipmap.i308;
                break;
            case "309":
                icon= R.mipmap.i309;
                break;
            case "310":
                icon= R.mipmap.i310;
                break;
            case "311":
                icon= R.mipmap.i311;
                break;
            case "312":
                icon= R.mipmap.i312;
                break;
            case "313":
                icon= R.mipmap.i313;
                break;
            case "314":
                icon= R.mipmap.i314;
                break;
            case "315":
                icon= R.mipmap.i315;
                break;
            case "316":
                icon= R.mipmap.i316;
                break;
            case "317":
                icon= R.mipmap.i317;
                break;
            case "318":
                icon= R.mipmap.i318;
                break;
            case "350":
                icon= R.mipmap.i350;
                break;
            case "351":
                icon= R.mipmap.i351;
                break;
            case "399":
                icon= R.mipmap.i399;
                break;
            case "400":
                icon= R.mipmap.i400;
                break;
            case "401":
                icon= R.mipmap.i401;
                break;
            case "402":
                icon= R.mipmap.i402;
                break;
            case "403":
                icon= R.mipmap.i403;
                break;
            case "404":
                icon= R.mipmap.i404;
                break;
            case "405":
                icon= R.mipmap.i405;
                break;
            case "406":
                icon= R.mipmap.i406;
                break;
            case "407":
                icon= R.mipmap.i407;
                break;
            case "408":
                icon= R.mipmap.i408;
                break;
            case "409":
                icon= R.mipmap.i409;
                break;
            case "410":
                icon= R.mipmap.i410;
                break;
            case "456":
                icon= R.mipmap.i456;
                break;
            case "457":
                icon= R.mipmap.i457;
                break;
            case "499":
                icon= R.mipmap.i499;
                break;
            case "500":
                icon= R.mipmap.i500;
                break;
            case "501":
                icon= R.mipmap.i501;
                break;
            case "502":
                icon= R.mipmap.i502;
                break;
            case "503":
                icon= R.mipmap.i503;
                break;
            case "504":
                icon= R.mipmap.i504;
                break;
            case "507":
                icon= R.mipmap.i507;
                break;
            case "508":
                icon= R.mipmap.i508;
                break;
            case "509":
                icon= R.mipmap.i509;
                break;
            case "510":
                icon= R.mipmap.i510;
                break;
            case "511":
                icon= R.mipmap.i511;
                break;
            case "512":
                icon= R.mipmap.i512;
                break;
            case "513":
                icon= R.mipmap.i513;
                break;
            case "514":
                icon= R.mipmap.i514;
                break;
            case "515":
                icon= R.mipmap.i515;
                break;
            case "900":
                icon= R.mipmap.i900;
                break;
            case "901":
                icon= R.mipmap.i901;
                break;
            case "999":
                icon= R.mipmap.i999;
                break;
        }
        return icon;
    }

    public int imageCheckBG(String text){

        int backGround=R.drawable.weather;

        switch (text){
            case "晴":
                backGround=R.drawable.sunny;
                break;
            case "多云":
                backGround=R.drawable.cloudy;
                break;
            case "阴":
                backGround=R.drawable.overcast;
                break;
        }

        if (text.indexOf("雨") != -1) {
            backGround=R.drawable.rain;
        }
        if (text.indexOf("雪" )!= -1) {
            backGround=R.drawable.snow;
        }
        if(text.indexOf("雷")!=-1){
            backGround=R.drawable.thunder;
        }
        if((text.indexOf("沙")!=-1)||(text.indexOf("尘")!=-1)||(text.indexOf("霾")!=-1)||(text.indexOf("雾")!=-1)){
            backGround=R.drawable.fog;
        }
        return backGround;

    }
}
