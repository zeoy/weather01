package com.example.weather01.ui.main;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.weather01.R;
import com.example.weather01.backen.BasicInfor.weather;
import com.example.weather01.ui.datatype.HourlyWeather;
import com.example.weather01.ui.datatype.SevenWeather;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *  调用后端weather 整合天气信息
 *
 *  城市信息
 *  未来七天
 *  未来24小时
 *  当天详细信息
 *
 */
public class WeatherInfo {

    public List<SevenWeather> getSevenDays(String CityName){
        ImageCheck imageCheck=new ImageCheck();
        List<SevenWeather> sevenWeather;
        weather weatherSevenDays=new weather();
        JSONArray sevenDays=new JSONArray();
        try {
            sevenDays=weatherSevenDays.get7DbyAreaName(CityName);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sevenWeather=new ArrayList<>();

        for(int count=0;count<7;count++){

            JSONObject dayText=sevenDays.getJSONObject(count);
            int dayIcon=R.mipmap.i100;
            dayIcon=imageCheck.imageCheck(dayText.getString("iconDay"));
            sevenWeather.add(new SevenWeather(sevenDays.getJSONObject(count).getString("fxDate"),sevenDays.getJSONObject(count).getString("tempMax"),sevenDays.getJSONObject(count).getString("tempMin"),dayIcon));
        }

        return sevenWeather;
    }

    public List<HourlyWeather> getHourlyWeather(String CityName){
        ImageCheck imageCheck=new ImageCheck();
        List<HourlyWeather> hourlyWeather=new ArrayList<>();
        weather weatherHourly=new weather();
        JSONArray Hourly=new JSONArray();
        try {
            Hourly=weatherHourly.getHourlybyAreaName(CityName);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int count=0;count<24;count++){
            JSONObject horlyText=Hourly.getJSONObject(count);
            int hourlyIcon=R.mipmap.i100;
            hourlyIcon=imageCheck.imageCheck(horlyText.getString("icon"));
            hourlyWeather.add(new HourlyWeather(Hourly.getJSONObject(count).getString("fxTime").substring(11,16),Hourly.getJSONObject(count).getString("temp"),hourlyIcon));
        }

        return hourlyWeather;
    }

    public String [] getWeatherInfo(String CityName) throws JSONException {
        String [] otherInfo;
        weather weatherInfo=new weather();
        JSONObject airInfo;
        JSONObject info;
        JSONObject nowInfo;
        JSONObject hourlyInfo;
        //空气质量 + 其他0-15 + 当前温度+ 当天最低最高温 + 当天天气情况
        airInfo=weatherInfo.getAirConditionbyAreaName(CityName);
        info=weatherInfo.get7DbyAreaName(CityName).getJSONObject(0);
        nowInfo=weatherInfo.getNowWeather(CityName);
        hourlyInfo=weatherInfo.getHourlybyAreaName(CityName).getJSONObject(0);
        String currentTemp=nowInfo.getString("temp")+"℃";
        String highLowTemp=info.getString("tempMax")+"/"+info.getString("tempMin")+"℃";
        String weatherText=nowInfo.getString("text");


//        List<HourlyWeather> hourlyWeather=new ArrayList<>();
//        weather weatherHourly=new weather();
//        JSONArray Hourly=new JSONArray();
//        try {
//            Hourly=weatherHourly.getHourlybyAreaName(CityName);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JSONObject horlyText=Hourly.getJSONObject(0);
        String Text=info.getString("textDay");
        System.out.println("背景背景："+Text);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");// HH:mm:ss
        //获取当前时间
        Date date = new Date(System.currentTimeMillis());


        //整合传参
        otherInfo= new String[]{airInfo.getString("pm10"),
                airInfo.getString("pm2p5"),
                airInfo.getString("no2"),
                airInfo.getString("so2"),
                airInfo.getString("o3"),
                airInfo.getString("co"),
                info.getString("sunrise"),
                info.getString("sunset"),
                //降雨概率
                hourlyInfo.getString("pop"),
                nowInfo.getString("temp"),
                nowInfo.getString("windDir"),
                nowInfo.getString("windSpeed"),
                nowInfo.getString("precip"),
                nowInfo.getString("pressure"),
                nowInfo.getString("vis"),
                nowInfo.getString("cloud"),
                currentTemp,
                highLowTemp,
                weatherText,
                simpleDateFormat.format(date),
                //每小时天气情况
                Text
        };
        return otherInfo;
    }
}
