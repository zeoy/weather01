package com.example.weather01.ui.datatype;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 未来N天预报数据列表
 * Parcelable序列化该类，方便后续使用RecycleView传参
 */

public class SevenWeather implements Parcelable {
    private String Date;
    private String TemHeight;
    private String TemLowest;
    private int DayIcon;

    public SevenWeather(String Date, String TemHeight, String TemLowest,int DayIcon){
        this.Date=Date;
        this.TemHeight=TemHeight;
        this.TemLowest=TemLowest;
        this.DayIcon=DayIcon;
    }

    public SevenWeather(Parcel in) {
        Date = in.readString();
        TemHeight = in.readString();
        TemLowest = in.readString();
        DayIcon = in.readInt();
    }

    public String getDate(){
        return Date;
    }

    public String getTemHeight(){
        return TemHeight;
    }

    public String getTemLowest(){
        return TemLowest;
    }

    public int getDayIcon(){
        return DayIcon;
    }

    public static final Creator<SevenWeather> CREATOR = new Creator<SevenWeather>() {
        @Override
        public SevenWeather createFromParcel(Parcel in) {
            return new SevenWeather(in);
        }

        @Override
        public SevenWeather[] newArray(int size) {
            return new SevenWeather[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Date);
        dest.writeString(TemHeight);
        dest.writeString(TemLowest);
        dest.writeInt(DayIcon);
    }
}
