package com.example.weather01.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weather01.R;
import com.example.weather01.backen.BasicInfor.GetLatAndLng;
import com.example.weather01.backen.Others.Recommend;
import com.example.weather01.ui.adapter.ShowHourlyAdapter;
import com.example.weather01.ui.adapter.ShowSevenAdapter;
import com.example.weather01.ui.datatype.HourlyWeather;
import com.example.weather01.ui.datatype.SevenWeather;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *主界面综合信息fragment
 */

public class WeatherFragment extends Fragment {

    ShowHourlyAdapter showHourlyAdapter;
    ShowSevenAdapter showSevenAdapter;

    private static List<HourlyWeather> hourlyWeathers=new ArrayList<>();
    private static List<SevenWeather> sevenWeathers=new ArrayList<>();

    private String [] otherInfo;
    private String [] cityInfo;

    private static final String PARAM = "CITYID";
    private static final String HOURLYINFO="HOURLYWEATHER";
    private static final String SEVRENINFO="SEVERNINFO";
    private static final String OTHERINFO="OTHERINFO";
    private static final String CITYINFO="CITYINFO";

    private PageViewModel pageViewModel;

    public static WeatherFragment newInstance(int cityID,String[] cityInfo,List<HourlyWeather> hourlyList,List<SevenWeather> sevenList,String[]  otherInfo) {
        WeatherFragment fragment = new WeatherFragment();
        Bundle bundle = new Bundle();

        bundle.putInt(PARAM,cityID);
        bundle.putSerializable(HOURLYINFO,(Serializable)hourlyList);
        bundle.putSerializable(SEVRENINFO,(Serializable)sevenList);
        bundle.putStringArray(OTHERINFO,otherInfo);
        bundle.putStringArray(CITYINFO,cityInfo);

        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = new ViewModelProvider(this).get(PageViewModel.class);
        int index = 0;
        if (getArguments() != null) {
            index = getArguments().getInt(PARAM);

            hourlyWeathers=(List<HourlyWeather>) getArguments().getSerializable(HOURLYINFO);
            showHourlyAdapter=new ShowHourlyAdapter(getActivity(),hourlyWeathers);

            sevenWeathers=(List<SevenWeather>)getArguments().getSerializable(SEVRENINFO);
            showSevenAdapter=new ShowSevenAdapter(getActivity(),sevenWeathers);

            otherInfo=(String[]) getArguments().getStringArray(OTHERINFO);
            cityInfo=(String[]) getArguments().getStringArray(CITYINFO);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_main, container, false);

        //逐小时预报适配器
        RecyclerView HourlyWeather= root.findViewById(R.id.ShowHourly);
        LinearLayoutManager managerHourly = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        HourlyWeather.setItemAnimator(new DefaultItemAnimator());
        HourlyWeather.setLayoutManager(managerHourly);
        HourlyWeather.setAdapter(showHourlyAdapter);

        //未来七天预报适配器
        RecyclerView SevenWeather= root.findViewById(R.id.ShowSeven);
        LinearLayoutManager managerSeven = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        SevenWeather.setItemAnimator(new DefaultItemAnimator());
        SevenWeather.setLayoutManager(managerSeven);
        SevenWeather.setAdapter(showSevenAdapter);

        //城市信息
        TextView CityName=root.findViewById(R.id.CityName);
        CityName.setText(cityInfo[0]+" "+cityInfo[1]);

        //空气质量
        TextView tv_pm10=root.findViewById(R.id.tv_pm10);
        tv_pm10.setText(otherInfo[0]);

        TextView tv_pm25=root.findViewById(R.id.tv_pm25);
        tv_pm25.setText(otherInfo[1]);

        TextView tv_no2=root.findViewById(R.id.tv_no2);
        tv_no2.setText(otherInfo[2]);

        TextView tv_so2=root.findViewById(R.id.tv_so2);
        tv_so2.setText(otherInfo[3]);

        TextView tv_o3=root.findViewById(R.id.tv_o3);
        tv_o3.setText(otherInfo[4]);

        TextView tv_co=root.findViewById(R.id.tv_co);
        tv_co.setText(otherInfo[5]);

        //其他信息
        TextView sunOut= root.findViewById(R.id.sunOut);
        sunOut.setText(otherInfo[6]);

        TextView sunDown= root.findViewById(R.id.sunDown);
        sunDown.setText(otherInfo[7]);

        TextView rainPercent= root.findViewById(R.id.rainPercent);
        rainPercent.setText(otherInfo[8]);

        TextView temBody= root.findViewById(R.id.temBody);
        temBody.setText(otherInfo[9]);

        TextView wind= root.findViewById(R.id.wind);
        wind.setText(otherInfo[10]);

        TextView windSpeed= root.findViewById(R.id.windSpeed);
        windSpeed.setText(otherInfo[11]);

        TextView precipitation= root.findViewById(R.id.precipitation);
        precipitation.setText(otherInfo[12]);

        TextView airPressure= root.findViewById(R.id.airPressure);
        airPressure.setText(otherInfo[13]);

        TextView visibility= root.findViewById(R.id.visibility);
        visibility.setText(otherInfo[14]);

        TextView UVIntensity= root.findViewById(R.id.UVIntensity);
        UVIntensity.setText(otherInfo[15]);

        TextView Temperature=root.findViewById(R.id.Temperature);
        Temperature.setText(otherInfo[16]);

        TextView HightToLow=root.findViewById(R.id.HightToLow);
        HightToLow.setText(otherInfo[17]);

        TextView Situation=root.findViewById(R.id.Situation);
        Situation.setText(otherInfo[18]);

        //时间
        TextView DayDate=root.findViewById(R.id.DayDate);
        DayDate.setText(otherInfo[19]);

        //根据天气情况选择背景图

        ImageCheck imageCheck=new ImageCheck();
        int backGround=imageCheck.imageCheckBG(otherInfo[18]);
        ImageView Background=root.findViewById(R.id.background);
        Background.setBackgroundResource(backGround);


        root.findViewById(R.id.testRecommend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), Recommend.class);
                Bundle bundle=new Bundle();
                bundle.putString("name", cityInfo[1]);
                bundle.putString("weather",otherInfo[18]);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        root.findViewById(R.id.testdelete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });


        final TextView textView = root.findViewById(R.id.section_label);
        pageViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore the fragment's state here
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save the fragment's state here
    }


}