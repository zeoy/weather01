package com.example.weather01.backen.Activity;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weather01.R;
import com.example.weather01.backen.BasicInfor.adapter.Area;
import com.example.weather01.backen.BasicInfor.adapter.City;
import com.example.weather01.backen.BasicInfor.adapter.CityAdapter;
import com.example.weather01.backen.BasicInfor.adapter.N_city;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class cityPickActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_pick);
        //把城市名传递给CityList
        String s=getJson("city.json");
        List<City> cityList = new ArrayList<City>();

        CityAdapter cityAdapter;
        try {
            JSONArray json = new JSONArray(s);


            for(int i=0;i<json.length();i++)
            {
                JSONArray jb=json.getJSONObject(i).getJSONArray("children");
                List<N_city> n_cityList = new ArrayList<N_city>();
                for(int j=0;j<jb.length();j++){
                    JSONArray jc=jb.getJSONObject(j).getJSONArray("children");
                    List<Area> areaList = new ArrayList<Area>();
                    for(int k=0;k<jc.length();k++){
                        JSONObject jd=jc.getJSONObject(k);
                        Area area=new Area(jd.getString("name"));
                        areaList.add(area);
                        System.out.println(area.a_name);
                    }
                    N_city n_city=new N_city(jb.getJSONObject(j).getString("name"),areaList);
                    n_cityList.add(n_city);
                    System.out.println("n_city"+n_city.n_name);
                }
                City city=new City(json.getJSONObject(i).getString("name"),n_cityList);
                cityList.add(city);
                System.out.println("city"+city.province);
            }
            System.out.println("封装结束");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        cityAdapter=new CityAdapter(getBaseContext(),cityList,this);
        recyclerView = findViewById(R.id.p_name);
        LinearLayoutManager managerSeven = new LinearLayoutManager(cityPickActivity.this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(managerSeven);
        recyclerView.setAdapter(cityAdapter);


        findViewById(R.id.p_name).setOnHoverListener(new View.OnHoverListener() {
            @Override
            public boolean onHover(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_HOVER_ENTER:
                        ;//鼠标悬停的selector
                        break;
                    case MotionEvent.ACTION_HOVER_EXIT:
                        v.setBackgroundResource(R.color.colorAccent);//没有鼠标时背景透明
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
    }

    //解析city.json获取城市名
    public String getJson(String fileName) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            AssetManager assetManager = cityPickActivity.this.getAssets();
            BufferedReader bf = new BufferedReader(new InputStreamReader(assetManager.open(fileName)));
            String line;
            while ((line = bf.readLine()) != null) {
                stringBuilder.append(line);
//                Log.d("AAA", line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(stringBuilder);
        return stringBuilder.toString();
    }

}