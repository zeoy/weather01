package com.example.weather01.backen.Others;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.example.weather01.R;
import com.example.weather01.ui.main.ImageCheck;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Recommend extends AppCompatActivity {
    public int count=0;//确保music在只点击暂停播放的时候只同步一次即只传入一个歌单
    public String name;//城市名
    Music music;
    private Uri uri1;//歌曲端口
    MediaPlayer mediaPlayer;
    String id;//歌单id
    Boolean flag = true;//穿搭指数
    Boolean flag2=true;//生活指数
    int no=1;//一个歌单中的歌曲序号

    @SuppressLint("SetTextI18n")//text内容长度
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommend);


        @SuppressLint({"NewApi", "LocalSuppress"})
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

//Adapter传入城市名
        Bundle bundle = new Bundle();
        bundle = this.getIntent().getExtras();
        name = bundle.getString("name");
        String weather =bundle.getString("weather");


        findViewById(R.id.backToMain).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImageCheck imageCheck=new ImageCheck();
        findViewById(R.id.recommend).setBackgroundResource(imageCheck.imageCheckBG(weather));


//音乐部分
        music = new Music();
        MusicNetwork musicNetwork=new MusicNetwork();

        //三个歌单晴天、下雨天、阴天
        String [] sunny_song = new String[3];
        String [] rainy_song = new String[3];
        String [] cloudy_song = new String[3];
        for(int i=0;i<3;i++){
            sunny_song[0]="5430407182";
            sunny_song[1]="3235927265";
            sunny_song[2]="2149243771";
            rainy_song[0]="423087650";
            rainy_song[1]="112771546";
            rainy_song[2]="719322568";
            cloudy_song[0]="3019153333";
            cloudy_song[1]="6692357390";
            cloudy_song[2]="3072254505";
        }

        //从主页面那里获取的天气名晴天、阴天、下雨天

        while (flag) {
            switch (weather) {
                case "晴":
                    id = sunny_song[(int) (Math.random() * 3)];
                    break;
                case "雨":
                    id = rainy_song[(int) (Math.random() * 3)];
                    break;
                case "阴":
                    id = cloudy_song[(int) (Math.random() * 3)];
                    break;
            }
            if((weather.indexOf("沙")!=-1)||(weather.indexOf("尘")!=-1)||(weather.indexOf("霾")!=-1)||(weather.indexOf("雾")!=-1)||(weather.indexOf("云")!=-1)){
                id = cloudy_song[(int) (Math.random() * 3)];
            }
            flag = false;
        }

        //根据歌单id获取音乐的所有信息
        musicNetwork.Cloud_Muisc_MusicListSearch(this,id, no,new MusicNetwork.VolleyCallback() {
            @Override
            public void onSuccess(Music m) {
                music=m;
                Log.i("success","音乐");
                //音乐信息
                TextView author = findViewById(R.id.author);
                TextView song = findViewById(R.id.song);
                String url = music.getPic();
                Bitmap bitmap = getHttpBitmap(url);
                ImageView pic =(ImageView) findViewById(R.id.song_img);
                pic.setImageBitmap(bitmap);
                author.setText(music.getAuthor());
                Log.i("音乐author",""+music.getAuthor());
                song.setText(music.getName());
                uri1 = Uri.parse(music.getUrl());
                mediaPlayer=new MediaPlayer();
                try {
                    mediaPlayer.setDataSource(Recommend.this, uri1);
                } catch (IOException e) {
                    @SuppressLint("WrongConstant") Toast toast =Toast.makeText(Recommend.this,"该歌曲没有版权。。QAQ,点击下一首试试看吧",10);
                    toast.show();
                    e.printStackTrace();
                }
            }
        });

        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }

        //播放
        TextView suspend = (FontIconView) findViewById(R.id.suspend);

        //播放及暂停
        findViewById(R.id.suspend).setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                count++;
//                v.setTooltipText("play");
//
//                Log.i("音乐","play");
                suspend.setText(getResources().getString(R.string.play));
                if (mediaPlayer.isPlaying()){
                    Log.i("音乐","isplaying");
                    //v.setBackgroundResource(R.string.suspend);
                    mediaPlayer.pause(); //音乐暂停
                    suspend.setText(getResources().getString(R.string.suspend));
                }
                else {
                    try {
                        if(count==1){
                        mediaPlayer.prepare();
                        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                mp.start();
                            }
                        });}
                        else
                            mediaPlayer.start();
//                    mediaPlayer.start();
//调用MediaPlayer的start()、pause()、stop()等方法控制
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });

        //上一首
        findViewById(R.id.previous).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                count=0;
                try {
                    no++;
                    musicNetwork.Cloud_Muisc_MusicListSearch(Recommend.this,id, no,new MusicNetwork.VolleyCallback() {
                        @Override
                        public void onSuccess(Music m) {
                            music=m;
                            Log.i("success","音乐");
                            //音乐信息
                            TextView author = findViewById(R.id.author);
                            TextView song = findViewById(R.id.song);
                            author.setText(music.getAuthor());
                            Log.i("音乐author",""+music.getAuthor());
                            song.setText(music.getName());
                            uri1 = Uri.parse(music.getUrl());
                            mediaPlayer=new MediaPlayer();
                            try {
                                mediaPlayer.setDataSource(Recommend.this, uri1);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //下一首
        findViewById(R.id.last).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    count=0;
                    no--;
                    musicNetwork.Cloud_Muisc_MusicListSearch(Recommend.this,id, no,new MusicNetwork.VolleyCallback() {
                        @Override
                        public void onSuccess(Music m) {
                            music=m;
                            Log.i("success","音乐");
                            //音乐信息
                            TextView author = findViewById(R.id.author);
                            TextView song = findViewById(R.id.song);
                            author.setText(music.getAuthor());
                            Log.i("音乐author",""+music.getAuthor());
                            song.setText(music.getName());
                            uri1 = Uri.parse(music.getUrl());
                            mediaPlayer=new MediaPlayer();
                            try {
                                mediaPlayer.setDataSource(Recommend.this, uri1);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


//生活指数和穿搭部分
        Life life = new Life();
        Outfits outfits = new Outfits();
        life.getAll(name);
        outfits.getAll(name);
        //生活指数
        TextView carWash=findViewById(R.id.carWash);
        TextView sport=findViewById(R.id.sport);
        TextView fishing=findViewById(R.id.fishing);
        TextView allergy=findViewById(R.id.allergy);
        TextView travel=findViewById(R.id.Travel);
        TextView comfort=findViewById(R.id.Comfort);
        TextView cold=findViewById(R.id.cold);
        TextView drying=findViewById(R.id.Airdrying);
        //穿搭指数
        TextView clothes=findViewById(R.id.clothes);
        TextView UV=findViewById(R.id.shine);
        TextView sunglasses=findViewById(R.id.sunglasses);
        TextView makeup=findViewById(R.id.makeup);
        TextView sunCream=findViewById(R.id.sunCream);
        //穿搭推荐
        TextView clothes_upper=findViewById(R.id.clothes_upper);
        TextView clothes_lower=findViewById(R.id.clothes_lower);
        TextView loose_coat=findViewById(R.id.loose_coat);
        TextView UV_info=findViewById(R.id.UV_info);
        TextView sunglasses_info=findViewById(R.id.sunglasses_info);
        TextView makeup_info=findViewById(R.id.makeup_info);
        TextView sunCream_info=findViewById(R.id.sunCream_info);

        //显示生活指数
        while (flag2){
            switch (life.getCarWash()) {
                case "1":
                    carWash.setText("适宜");
                    break;
                case "2":
                    carWash.setText("较适宜");
                    break;
                case "3":
                    carWash.setText("较不宜");
                    break;
                case "4":
                    carWash.setText("不宜");
                    break;
            }
            switch (life.getSport()) {
                case "1":
                    sport.setText("适宜");
                    break;
                case "2":
                    sport.setText("较适宜");
                    break;
                case "3":
                    sport.setText("较不宜");
                    break;
            }
            switch (life.getFishing()) {
                case "1":
                    fishing.setText("适宜");
                    break;
                case "2":
                    fishing.setText("较适宜");
                    break;
                case "3":
                    fishing.setText("不宜");
                    break;
            }
            switch (life.getTravel()) {
                case "1":
                    travel.setText("适宜");
                    break;
                case "2":
                    travel.setText("较适宜");
                    break;
                case "3":
                    travel.setText("一般");
                    break;
                case "4":
                    travel.setText("较不宜");
                    break;
                case "5":
                    travel.setText("不适宜");
                    break;
            }
            switch (life.getComfort()) {
                case "1":
                    comfort.setText("舒适");
                    break;
                case "2":
                    comfort.setText("较舒适");
                    break;
                case "3":
                    comfort.setText("较不舒适");
                    break;
                case "4":
                    comfort.setText("很不舒适");
                    break;
                case "5":
                    comfort.setText("极不舒适");
                    break;
                case "6":
                    comfort.setText("不舒适");
                    break;
                case "7":
                    comfort.setText("非常不舒适");
                    break;
            }
            switch (life.getAllergy()) {
                case "1":
                    allergy.setText("极不易发");
                    break;
                case "2":
                    allergy.setText("不易发");
                    break;
                case "3":
                    allergy.setText("较易发");
                    break;
                case "4":
                    allergy.setText("易发");
                    break;
                case "5":
                    allergy.setText("极易发");
                    break;
            }
            switch (life.getCold()) {
                case "1":
                    cold.setText("少发");
                    break;
                case "2":
                    cold.setText("较易发");
                    break;
                case "3":
                    cold.setText("易发");
                    break;
                case "4":
                    cold.setText("极易发");
                    break;
            }
            switch (life.getDrying()) {
                case "1":
                    drying.setText("极适宜");
                    break;
                case "2":
                    drying.setText("适宜");
                    break;
                case "3":
                    drying.setText("基本适宜");
                    break;
                case "4":
                    drying.setText("不太适宜");
                    break;
                case "5":
                    drying.setText("不宜");
                    break;
                case "6":
                    drying.setText("不适宜");
                    break;
            }

            //穿衣推荐返回值
            switch (outfits.getClothes()){
                case "1":
                    clothes.setText("寒冷");
                    clothes_upper.setText("上装：保暖打底，加厚毛衣、卫衣");
                    clothes_lower.setText("下装：棉裤、加绒长裤");
                    loose_coat.setText("外套:厚羽绒服");
                    break;
                case "2":
                    clothes.setText("冷");
                    clothes_upper.setText("上装：加厚毛衣、卫衣");
                    clothes_lower.setText("下装：加厚长裤");
                    loose_coat.setText("外套: 薄羽绒服、棉服");
                    break;
                case "3":
                    clothes.setText("较冷");
                    clothes_upper.setText("上装：毛衣、卫衣");
                    clothes_lower.setText(null);
                    loose_coat.setText("外套: 大衣、毛呢外套、厚外套");
                    break;
                case "4":
                    clothes.setText("较舒适");
                    clothes_upper.setText("上装：薄毛衣、针织衫");
                    clothes_lower.setText("下装：牛仔裤");
                    loose_coat.setText("外套: 薄外套、开衫、薄夹克");
                    break;
                case "5":
                    clothes.setText("舒适");
                    clothes_upper.setText("上装：长袖T恤、衬衫");
                    clothes_lower.setText("下装：单裤");
                    loose_coat.setText("外套: 针织外套、马甲（选择性）");
                    break;
                case "6":
                    clothes.setText("热");
                    clothes_upper.setText("上装：T恤");
                    clothes_lower.setText("下装：短裙、短裤");
                    loose_coat.setText("外套: 防晒外套（选择性）");
                    break;
                case "7":
                    clothes.setText("炎热");
                    clothes_upper.setText("上装：短衫、背心、薄T恤");
                    clothes_lower.setText("下装：短裙、短裤");
                    loose_coat.setText("外套: 防晒外套（选择性）");
                    break;
            }
            switch (outfits.getUV()){
                case "1":
                    UV.setText("最弱");
                    UV_info.setText("防晒：属弱紫外线辐射天气，无需特别防护。若长期在户外，建议涂擦SPF在8-12之间的防晒护肤品");
                    break;
                case "2":
                    UV.setText("弱");
                    UV_info.setText("防晒：紫外线强度较弱，建议出门前涂擦SPF在12-15之间、PA+的防晒护肤品");
                    break;
                case "3":
                    UV.setText("中等");
                    UV_info.setText("防晒：属中等强度紫外线辐射天气，外出时建议涂擦SPF高于15、PA+的防晒护肤品，戴帽子、太阳镜");
                    break;
                case "4":
                    UV.setText("强");
                    UV_info.setText("防晒：紫外线辐射强，建议涂擦SPF20左右、PA++的防晒护肤品。避免在10点至14点暴露于日光下");
                    break;
                case "5":
                    UV.setText("很强");
                    UV_info.setText("防晒：紫外线辐射极强，建议涂擦SPF20以上、PA++的防晒护肤品，尽量避免暴露于日光下");
                    break;
            }
            switch (outfits.getSunglasses()){
                case "1":
                    sunglasses.setText("不需要");
                    sunglasses_info.setText("太阳镜：白天有降水天气，视线较差，不需要佩戴太阳镜；白天天空阴沉，户外光线较暗，不需要佩戴太阳镜\n");
                    break;
                case "2":
                    sunglasses.setText("需要");
                    sunglasses_info.setText("太阳镜：白天根据户外光线情况，适时佩戴太阳镜");
                    break;
                case "3":
                    sunglasses.setText("必要");
                    sunglasses_info.setText("太阳镜：白天太阳辐射较强，建议佩戴透射比1级且标注UV380-UV400的浅色太阳镜");
                    break;
                case "4":
                    sunglasses.setText("很必要");
                    sunglasses_info.setText(null);
                    break;
                case "5":
                    sunglasses.setText("非常必要");
                    sunglasses_info.setText(null);
                    break;
            }
            switch (outfits.getMakeup()){
                case "1":
                    makeup.setText("保湿");
                    makeup_info.setText("化妆品：皮肤易缺水，用润唇膏后再抹口红，用保湿型霜类化妆品");
                    break;
                case "2":
                    makeup.setText("保湿防晒");
                    makeup_info.setText("化妆品：润唇膏、保湿性面霜、带防晒指数的粉底");
                    break;
                case "3":
                    makeup.setText("去油防晒");
                    makeup_info.setText("化妆品：防晒隔离、水质无油粉底，透明粉饼");
                    break;
                case "4":
                    makeup.setText("防脱水防晒");
                    makeup_info.setText("化妆品：防汗防脱妆化妆品，轻薄粉底，常补粉");
                    break;
                case "5":
                    makeup.setText("去油");
                    makeup_info.setText("化妆品：天气较热，建议用露质面霜打底，水质无油粉底霜，透明粉饼，粉质胭脂");
                    break;
                case "6":
                    makeup.setText("防脱水");
                    makeup_info.setText("化妆品：天气较热，易出汗，建议使用防脱水化妆品，少用粉底和胭脂，经常补粉");
                    break;
                case "7":
                    makeup.setText("防晒");
                    makeup_info.setText("化妆品：温湿适宜，但最好使用SPF15以上防晒霜打底，建议使用中性保湿型化妆品");
                    break;
                case "8":
                    makeup.setText("滋润保湿");
                    makeup_info.setText("化妆品：天气较冷、空气干燥，用滋润保湿型化妆品，少扑粉，使用润唇膏");
                    break;
            }
            switch (outfits.getSunCream()){
                case "1":
                    sunCream.setText("弱");
                    sunCream_info.setText("防晒霜：SPF在8-12之间的防晒护肤品");
                    break;
                case "2":
                    sunCream.setText("较弱");
                    sunCream_info.setText("防晒霜：SPF在12-15之间，PA+的防晒护肤品");
                    break;
                case "3":
                    sunCream.setText("中等");
                    sunCream_info.setText("防晒霜：SPF指数高于15，PA+的防晒护肤品");
                    break;
                case "4":
                    sunCream.setText("强");
                    sunCream_info.setText("防晒霜：SPF20，PA++");
                    break;
                case "5":
                    sunCream.setText("极强");
                    sunCream_info.setText("防晒霜：SPF30，PA+++");
                    break;
            }
            flag2 = false;
        }

    }


    /**
     * 获取网落图片资源
     * @param url
     * @return
     */
    public static Bitmap getHttpBitmap(String url){
        URL myFileURL;
        Bitmap bitmap=null;
        try{
            myFileURL = new URL(url);
            //获得连接
            HttpURLConnection conn=(HttpURLConnection)myFileURL.openConnection();
            //设置超时时间为6000毫秒，conn.setConnectionTiem(0);表示没有时间限制
            conn.setConnectTimeout(6000);
            //连接设置获得数据流
            conn.setDoInput(true);
            //不使用缓存
            conn.setUseCaches(false);
            //这句可有可无，没有影响
            //conn.connect();
            //得到数据流
            InputStream is = conn.getInputStream();
            //解析得到图片
            bitmap = BitmapFactory.decodeStream(is);
            //关闭数据流
            is.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return bitmap;
    }

}
