package com.example.weather01.backen.BasicInfor;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.example.weather01.backen.http.HttpGET;

import java.net.URL;

//通过经纬度获取地区的详情
public class LocationLL {
    //通过经纬度得到location的接口url
    public String url_location = "http://api.map.baidu.com/reverse_geocoding/v3/?";
    public String key = "ak=nXyAXdETM2NjnmuABSvXSvliRvxGb5Em";
    public String format1 = "&output=json&coordtype=wgs84ll&location=";
    public String format2 = "&oe=utf-8&format=json";

    //得到该经纬度对应地区的城市详情
    public JSONObject getLocationFromLL(String latitude,String longitude) throws JSONException {
        String detailAddress=latitude+","+longitude;
        JSONObject addressComponent=new JSONObject();
        String url=url_location + key +format1+detailAddress +format2;
        //Log.i("url",url);
        HttpGET get = new HttpGET(url);
        if (!get.re.getString("status").equals("0")) {
            System.out.println(get.re.getString("status"));
            System.out.println("地址返回失败");
            return null;
        } else {
            JSONObject temp = (JSONObject) get.re.get("result");
            addressComponent = temp.getJSONObject("addressComponent");
            String province=addressComponent.getString("province");
            String city = addressComponent.getString("city");
            String district=addressComponent.getString("district");

            System.out.println("province = " + province);
            System.out.println("city = " + city);
            System.out.println("district = " + district);
            //主要信息
//            addressComponent.getString("country");//国家
//            addressComponent.getString("province");//省
//            addressComponent.getString("city");//市
//            addressComponent.getString("district");//区
//            addressComponent.getString("street");//街道
//            addressComponent.getString("street_number");//号
            //返回的都是String的值
        }
        return addressComponent;
    }
}