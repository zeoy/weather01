package com.example.weather01.backen.BasicInfor;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.fastjson.JSONObject;
import com.example.weather01.MainActivity;
import com.example.weather01.R;
import com.example.weather01.backen.Activity.cityPickActivity;
import com.example.weather01.backen.Others.Recommend;
import com.example.weather01.ui.datatype.HourlyWeather;
import com.example.weather01.ui.datatype.SevenWeather;
import com.example.weather01.ui.main.SectionsPagerAdapter;
import com.example.weather01.ui.main.WeatherFragment;
import com.example.weather01.ui.main.WeatherInfo;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * 定位Activity
 * fragment入口
 */

public class GetLatAndLng extends AppCompatActivity {
    public static String latitude;
    public static String longitude;

    public String Province;
    public String District;

    private List<Fragment> fragments;


    private List<HourlyWeather> hourlyWeathers2;
    private List<HourlyWeather> hourlyWeather = new ArrayList<>();
    private List<SevenWeather> sevenWeathers2;
    private List<SevenWeather> sevenWeather = new ArrayList<>();
    private WeatherInfo weatherInfo = new WeatherInfo();

    private String name;
    private String n_cityname;
    private int count=2;

    public static boolean isfinish = false;

    TextView tvtemptrture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String TAG = "ababa";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.testAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GetLatAndLng.this, cityPickActivity.class);
                startActivity(intent);
            }
        });


        Intent intent = getIntent();
        name = intent.getStringExtra("name");//接收String类型的值
        n_cityname=intent.getStringExtra("n_cityname");
        System.out.println("所选城市：" + name);
        getLocation();
    }

    //设置监听器
    public final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            try {
                updateToNewLocation(location);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            try {
                updateToNewLocation(null);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    //调取成功之后的操作
    private void updateToNewLocation(Location location) throws JSONException {
        if (location != null) {
            latitude = String.valueOf(location.getLatitude());
            longitude = String.valueOf(location.getLongitude());
            System.out.println("纬度" + latitude);
            System.out.println("经度" + longitude);
            isfinish = true;

            //调用城市接口，传入定位结果纬度和经度，得到具体城市
            LocationLL locationLL = new LocationLL();
            JSONObject address = locationLL.getLocationFromLL(latitude, longitude);
            String country = address.getString("country");
            String province = address.getString("province");
            String city = address.getString("city");
            String district = address.getString("district");

            District = address.getString("district");
            Province = address.getString("province");
            // 函数调用举例：返回jsonObject或jsonarray
            tvtemptrture = findViewById(R.id.temp);


            String[] cityInfo = {Province, District};

            sevenWeather = weatherInfo.getSevenDays(District);
            hourlyWeather = weatherInfo.getHourlyWeather(District);

            String[] otherInfo1 = new String[21];
            try {
                otherInfo1 = weatherInfo.getWeatherInfo(District);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            /**
             * fragment数量=用户所选城市数量
             */
            fragments = new ArrayList<>();
            WeatherFragment p1 = WeatherFragment.newInstance(1, cityInfo, hourlyWeather, sevenWeather, otherInfo1);

            fragments.add(p1);

            if (name != null) {
                String[] cityInfo2 = {n_cityname, name};
                String[] otherInfo2 = new String[21];
                otherInfo2 = weatherInfo.getWeatherInfo(name);

                hourlyWeathers2 = new ArrayList<>();
                hourlyWeathers2 = weatherInfo.getHourlyWeather(name);

                sevenWeathers2 = weatherInfo.getSevenDays(name);

                WeatherFragment p2 = WeatherFragment.newInstance(count, cityInfo2, hourlyWeathers2, sevenWeathers2, otherInfo2);
                count++;
                fragments.add(p2);
            }

            /**
             * 轮播组件
             */
            SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), fragments);
            ViewPager viewPager = findViewById(R.id.view_pager);
            viewPager.setAdapter(sectionsPagerAdapter);
            TabLayout tabs = findViewById(R.id.tabs);
            tabs.setupWithViewPager(viewPager);

        }
    }

    //判断权限，调用接口
    public void getLocation() {
        //创建位置管理器对象
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //检测权限
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {//未开启定位权限
            //开启定位权限,200是标识码
            Log.i("aaaa", "no");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 200);
        } else {
            //startLocaion();//开始定位
            Log.i("aaaa", "yes");
            Toast.makeText(this, "已开启定位权限", Toast.LENGTH_LONG).show();
        }

        //提供者
        String provider;
        //获取可以定位的所有提供者
        List<String> providerList = locationManager.getProviders(true);
        if (providerList.contains(LocationManager.GPS_PROVIDER)) {
            provider = LocationManager.GPS_PROVIDER;
            locationManager.requestLocationUpdates(provider, Integer.MAX_VALUE, 0, mLocationListener);
        }
        if (providerList.contains(LocationManager.NETWORK_PROVIDER)) {
            provider = LocationManager.NETWORK_PROVIDER;
            locationManager.requestLocationUpdates(provider, Integer.MAX_VALUE, 0, mLocationListener);
        }
        if (providerList.contains(LocationManager.PASSIVE_PROVIDER)) {
            provider = LocationManager.PASSIVE_PROVIDER;
            locationManager.requestLocationUpdates(provider, Integer.MAX_VALUE, 0, mLocationListener);
        }

    }


}
