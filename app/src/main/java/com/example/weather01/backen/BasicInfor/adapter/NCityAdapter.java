package com.example.weather01.backen.BasicInfor.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weather01.R;
import com.example.weather01.backen.Activity.areaPickActivity;

import java.io.Serializable;
import java.util.List;

/**
 * 逐小时预报数据列表适配器
 *
 */

public class NCityAdapter extends RecyclerView.Adapter<NCityAdapter.ViewHolder> {

    private List<N_city> list;
    Context context;
    Activity activity;


    public NCityAdapter(Context context, List<N_city> list, Activity activity){
        this.list=list;
        this.context=context;
        this.activity=activity;
    }


    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView p_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            p_name=itemView.findViewById(R.id.p_name);
        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView=View.inflate(parent.getContext(), R.layout.recycle_city_list,null);
        ViewHolder holder=new ViewHolder(itemView);
        TextView nname=holder.p_name;
        nname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name=nname.getText().toString();
                int id=holder.getPosition();
                List<Area> a_cityList= list.get(id).areaList;
                Intent intent =new Intent(activity, areaPickActivity.class);
                Bundle bundle=new Bundle();
                bundle.putSerializable("cityList", (Serializable) a_cityList);
                bundle.putString("n_cityname",list.get(id).n_name);
                intent.putExtras(bundle);
                activity.startActivity(intent);

            }
        });
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        N_city city=list.get(position);
        holder.p_name.setText(city.n_name);

    }


    @Override
    public int getItemCount() {
        return list!=null?list.size():0;
    }
}

