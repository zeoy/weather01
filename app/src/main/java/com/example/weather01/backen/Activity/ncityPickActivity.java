package com.example.weather01.backen.Activity;

import android.os.Bundle;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weather01.R;
import com.example.weather01.backen.BasicInfor.adapter.NCityAdapter;
import com.example.weather01.backen.BasicInfor.adapter.N_city;

import java.util.List;

public class ncityPickActivity extends cityPickActivity {
    List<N_city> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_pick);
        Bundle bundle=new Bundle();
        bundle=this.getIntent().getExtras();
        list= (List<N_city>) bundle.getSerializable("cityList");

        System.out.println(list.size());
        NCityAdapter nCityAdapter = new NCityAdapter(getBaseContext(),list,this);
        RecyclerView recyclerView = findViewById(R.id.p_name);
        LinearLayoutManager managerSeven = new LinearLayoutManager(ncityPickActivity.this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(managerSeven);
        recyclerView.setAdapter(nCityAdapter);

    }
}