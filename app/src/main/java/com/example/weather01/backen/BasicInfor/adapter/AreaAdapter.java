package com.example.weather01.backen.BasicInfor.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weather01.R;
import com.example.weather01.backen.BasicInfor.GetLatAndLng;
import com.example.weather01.backen.Others.Recommend;

import java.util.List;

/**
 * 逐小时预报数据列表适配器
 *
 */

public class AreaAdapter extends RecyclerView.Adapter<AreaAdapter.ViewHolder> {


    private List<Area> list;
    Context context;
    Activity activity;
    String n_cityname;

    public AreaAdapter(Context context, List<Area> list, Activity activity,String n_cityname){
        this.list=list;
        this.context=context;
        this.activity=activity;
        this.n_cityname=n_cityname;
    }


    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView p_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            p_name=itemView.findViewById(R.id.p_name);
        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView=View.inflate(parent.getContext(), R.layout.recycle_city_list,null);
        ViewHolder holder=new ViewHolder(itemView);
        TextView pname=holder.p_name;
        pname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name=pname.getText().toString();
                Intent intent =new Intent(activity, GetLatAndLng.class);
                Bundle bundle=new Bundle();
                bundle.putString("name", name);
                bundle.putString("n_cityname",n_cityname);
                intent.putExtras(bundle);
                activity.startActivity(intent);
            }
        });
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Area city=list.get(position);
        holder.p_name.setText(city.a_name);

    }


    @Override
    public int getItemCount() {
        return list!=null?list.size():0;
    }
}

