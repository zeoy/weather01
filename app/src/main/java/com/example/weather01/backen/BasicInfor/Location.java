package com.example.weather01.backen.BasicInfor;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.example.weather01.backen.http.HttpGET;
import com.qweather.sdk.bean.geo.GeoBean;

public class Location {
    //通过地区名得到location的接口url
    public String key="&key=bc3c26aacf6b45e28c5ef685dbf9835d";
    public String url_location="https://geoapi.qweather.com/v2/city/lookup?location=";

    //得到该地区的经纬度
    public JSONObject getLocation(String areaname) throws JSONException {
        JSONObject location=new JSONObject();
        HttpGET get=new HttpGET(url_location+areaname+key);
       if(!get.re.getString("code").equals("200")){
           System.out.println(get.re.getString("code"));
           System.out.println("地址返回失败");
           return null;
       }else {
           JSONArray temp = (JSONArray) get.re.get("location");
           location=temp.getJSONObject(0);
           //感觉主要就需要这些信息
//           location.getString("adm1");//省
//           location.getString("adm2");//市
//           location.getString("country");//国家
//           location.getString("id");//市ID
//           location.getString("name");//地区名
//           location.getString("lat");//纬度
//           location.getString("lon");//经度
           //返回的都是String的值
       }
        return location;

    }



}
