package com.example.weather01.backen.Others;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;


import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * 音乐网络类
 * Created by SmileSB101 on 2016/11/1 0001.
 */

public class MusicNetwork {
    /**
     * 网易音乐搜索API
     * http://s.music.163.com/search/get/
     * 获取方式：GET
     * 参数：
     * src: lofter //可为空
     * type: 1
     * filterDj: true|false //可为空
     * s: //关键词
     * limit: 10 //限制返回结果数
     * offset: 0 //偏移
     * callback: //为空时返回json，反之返回jsonp callback
     * @param s
     * @param context
     * @return
     * 注意废数字才用‘’符号，要不不能用，否则出错！！
     */
    public static void SearchMusic(Context context,String s,int limit,int type,int offset){
        String url = UrlConstants.CLOUD_MUSIC_API_SEARCH + "type="+type+"&s='" + s + "'&limit="+limit+"&offset="+offset;
        RequestQueue requestQueue = InternetUtill.getmRequestqueue(context);
        StringRequest straingRequest = new StringRequest(url,new Response.Listener<String>(){
            @Override
            public void onResponse(String s){
                try {
                    JSONObject json = new JSONObject(s);
                    Log.i("onResponse: ",json.toString());
                    } catch(JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError){
                Log.i("onResponse: ",volleyError.toString());
            }
        });
        requestQueue.add(straingRequest);
    }

    /**
     * 网易云音乐歌曲信息API
     * @param context
     * @param id 歌曲id
     * @param ids 用[]包裹起来的歌曲id 写法%5B %5D
     * @return
     */
    public static void Cloud_Music_MusicInfoAPI(Context context,String id,String ids)
    {
        String url = UrlConstants.CLOUD_MUSIC_API_MUSICINGO + "id="+id+"&ids=%5B"+ids+"%5D";
        RequestQueue requestQueue = InternetUtill.getmRequestqueue(context);
        StringRequest straingRequest = new StringRequest(url,new Response.Listener<String>(){
            @Override
            public void onResponse(String s){
                try {
                    JSONObject json = new JSONObject(s);
                    Log.i("onResponse: ",json.toString());
                } catch(JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError){
                Log.i("onResponse: ",volleyError.toString());
            }
        });
        requestQueue.add(straingRequest);
    }

    /**
     * 获取歌曲歌词的API
     *URL：
     GET http://music.163.com/api/song/lyric
     必要参数：
     id：歌曲ID
     lv：值为-1，我猜测应该是判断是否搜索lyric格式
     kv：值为-1，这个值貌似并不影响结果，意义不明
     tv：值为-1，是否搜索tlyric格式
     * @param context
     * @param os
     * @param id
     */
    public static void Cloud_Muisc_getLrcAPI(Context context,String os,String id)
    {
        String url = UrlConstants.CLOUD_MUSIC_API_MUSICLRC + "os="+os+"&id="+id+"&lv=-1&kv=-1&tv=-1";
        RequestQueue requestQueue = InternetUtill.getmRequestqueue(context);
        StringRequest straingRequest = new StringRequest(url,new Response.Listener<String>(){
            @Override
            public void onResponse(String s){
                try {
                    JSONObject json = new JSONObject(s);
                    Log.i("onResponse: ",json.toString());
                } catch(JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError){
                Log.i("onResponse: ",volleyError.toString());
            }
        });
        requestQueue.add(straingRequest);
    }

    /**
     * 获取歌单的API
     * @param context
     * @param id 歌单ID
     */

    public void Cloud_Muisc_MusicListSearch(Context context,String id, int no,final VolleyCallback callback)
    {

        String url1 = "https://music.163.com/api/v6/playlist/detail?" + "id="+id+"&n=100000&s=8";
        RequestQueue requestQueue = InternetUtill.getmRequestqueue(context);
        Log.i("trytry",url1);
        StringRequest straingRequest = new StringRequest(url1,new Response.Listener<String>(){
            @Override
            public void onResponse(String s){
                try {
                    String id2;
                    JSONObject json = new JSONObject(s);

                    JSONArray jsonArray=new JSONArray();
                    jsonArray=json.getJSONObject("playlist").getJSONArray("tracks");
//                    Log.i("拆分",jsonArray.getJSONObject(0).getString("id"));
//                    int no=1;
                    id2 =jsonArray.getJSONObject(no).getString("id");
                    String url="https://api.imjad.cn/cloudmusic/?type=song&id="+id2+"&br=128000&raw=true";
                    String name=jsonArray.getJSONObject(no).getString("name");
                    JSONObject al=jsonArray.getJSONObject(no).getJSONObject("al");
                    String pic=al.getString("picUrl");//图片路径，例http://p3.music.126.net/ewdhHv8vBNltMjh9N9BVxQ==/109951165670823097.jpg
                    JSONArray jsonArray2=new JSONArray();
                    jsonArray2=jsonArray.getJSONObject(no).getJSONArray("ar");
                    String author=jsonArray2.getJSONObject(0).getString("name");

                    Music music=new Music();
                    music.setId(id2);
                    music.setUrl(url);
                    music.setName(name);
                    music.setPic(pic);
                    music.setAuthor(author);
                    callback.onSuccess(music);
                } catch(JSONException e) {
                    Log.i("Cloud_Muisc_MusicListSearch","fail");
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError){
                Log.i("onResponse: ",volleyError.toString());
            }
        });
        requestQueue.add(straingRequest);
//        return song[0];
    }
    public interface VolleyCallback {
        void onSuccess(Music m);
    }
    public static JSONObject json = null;
    public static JSONObject getInfoFromUrl_Volley(String url,Context context)
    {
        json = null;
        RequestQueue requestQueue = InternetUtill.getmRequestqueue(context);
        StringRequest straingRequest = new StringRequest(url,new Response.Listener<String>(){
            @Override
            public void onResponse(String s){
                try {
                    json = new JSONObject(s);
                    Log.i("onResponse: ",json.toString());
                } catch(JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError){
                Log.i("onResponse: ",volleyError.toString());
            }
        });
        requestQueue.add(straingRequest);
        return json;
    }
    public class UrlConstants {
        /**
         * 云音乐搜索API网址
         */
        public static final String CLOUD_MUSIC_API_SEARCH = "http://s.music.163.com/search/get/?";
        /**
         * 歌曲信息API网址
         */
        public static final String CLOUD_MUSIC_API_MUSICINGO = "http://music.163.com/api/song/detail/?";
        /**
         * 获取歌曲的歌词
         */
        public static final String CLOUD_MUSIC_API_MUSICLRC = "http://music.163.com/api/song/lyric?";
        /**
         * 获取歌单
         */
        public static final String CLOUD_MUSIC_API_MUSICLIST = "http://music.163.com/api/playlist/detail?";
    }
}
