package com.example.weather01.backen.BasicInfor.adapter;

import java.io.Serializable;
import java.util.List;

public class N_city implements Serializable{

        public String n_name;
        public List<Area> areaList;

    public N_city(String name, List<Area> areaList) {
        this.n_name=name;
        this.areaList=areaList;
    }

    public List<Area> getAreaList() {
            return areaList;
        }

        public String getN_name() {
            return n_name;
        }


}
