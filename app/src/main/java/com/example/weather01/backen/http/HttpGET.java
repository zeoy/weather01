package com.example.weather01.backen.http;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
//通过GET Http接口的json数据。
public class HttpGET {

    public HttpGET(String  Url){
        this.re=getJson(Url);
    }
   public JSONObject re;
   public JSONObject getJson(String  Url){
        JSONObject result=new JSONObject();
        try {

            URL url = new URL(Url);
            StringBuffer document = new StringBuffer();
            //创建链接
            URLConnection conn = url.openConnection();
            //读取返回结果集
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"));
            String line = null;

            while ((line = reader.readLine()) != null){
                document.append(line);
            }
            reader.close();
            System.out.println(document);
            result= (JSONObject) JSON.parse(String.valueOf(document));
            System.out.println("GET结束");

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
