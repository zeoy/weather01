package com.example.weather01.backen.Others;

import com.alibaba.fastjson.JSONArray;
import com.example.weather01.backen.BasicInfor.Indices;

public class Outfits {
    private String clothes;//穿衣厚度

    private String UV;//紫外线强度

    private String sunglasses;//太阳镜指数

    private String makeup;//化妆指数

    private String sunCream;//防晒霜指数

    public void getAll(String areaName){
        Indices indices=new Indices();
        JSONArray jsonArray=new JSONArray();
        jsonArray=indices.getIndicesbyAreaName(areaName);
        //获得紫外线级别
        clothes=jsonArray.getJSONObject(2).getString("level");
        UV=jsonArray.getJSONObject(4).getString("level");
        sunglasses=jsonArray.getJSONObject(11).getString("level");
        makeup=jsonArray.getJSONObject(12).getString("level");
        sunCream=jsonArray.getJSONObject(15).getString("level");
    }
    public String getUV() {
         return UV;
    }

    public String getClothes() {
        return clothes;
    }

    public String getMakeup() {
        return makeup;
    }

    public String getSunCream() {
        return sunCream;
    }

    public String getSunglasses() {
        return sunglasses;
    }
}

