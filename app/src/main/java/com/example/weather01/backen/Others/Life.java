package com.example.weather01.backen.Others;

import com.alibaba.fastjson.JSONArray;
import com.example.weather01.backen.BasicInfor.Indices;

//生活指数
public class Life {


    private String carWash;//洗车：不宜

    private String sport;//运动：适宜
    private String fishing;//钓鱼
    private String allergy;//过敏预报：易发程度
    private String travel;//旅游
    private String comfort;//舒适度指数
    private String cold;//感冒
    private String drying;//晾晒
    private String traffic;//交通
    public void getAll(String areaName){
        Indices indices=new Indices();
        JSONArray jsonArray=new JSONArray();
        jsonArray=indices.getIndicesbyAreaName(areaName);
        //获得紫外线级别
        sport=jsonArray.getJSONObject(0).getString("level");
        carWash=jsonArray.getJSONObject(1).getString("level");
        fishing=jsonArray.getJSONObject(3).getString("level");
        travel=jsonArray.getJSONObject(5).getString("level");
        allergy=jsonArray.getJSONObject(6).getString("level");
        comfort=jsonArray.getJSONObject(7).getString("level");
        cold=jsonArray.getJSONObject(8).getString("level");
        drying=jsonArray.getJSONObject(13).getString("level");
        traffic=jsonArray.getJSONObject(14).getString("level");
    }

    public String getAllergy() {
        return allergy;
    }

    public String getCarWash() {
        return carWash;
    }

    public String getCold() {
        return cold;
    }

    public String getComfort() {
        return comfort;
    }

    public String getDrying() {
        return drying;
    }

    public String getFishing() {
        return fishing;
    }

    public String getSport() {
        return sport;
    }

    public String getTraffic() {
        return traffic;
    }

    public String getTravel() {
        return travel;
    }

}
