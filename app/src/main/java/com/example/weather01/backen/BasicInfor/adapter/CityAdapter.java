package com.example.weather01.backen.BasicInfor.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weather01.R;
import com.example.weather01.backen.Activity.ncityPickActivity;

import java.io.Serializable;
import java.util.List;

/**
 * 逐小时预报数据列表适配器
 *
 */

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.ViewHolder> {
    private List<City> list;
    Context context;
    Activity activity;
    private OnItemClickListener mOnItemClickListener;


    public CityAdapter(Context context, List<City> list, Activity activity){
        this.list=list;
        this.context=context;
        this.activity=activity;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView p_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            p_name=itemView.findViewById(R.id.p_name);
        }


    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView=View.inflate(parent.getContext(), R.layout.recycle_city_list,null);
        ViewHolder holder=new ViewHolder(itemView);
        TextView pname=holder.p_name;

        //View view = inflater.inflate(R.layout.item_layout, parent, false);
        //ViewHolder myViewHolder = new MyViewHolder(view);
//        itemView.setOnClickListener((View.OnClickListener) this);//将创建的Vie注册点击事件
        //return myViewHolder;

 

        pname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name=pname.getText().toString();
                int id =holder.getAdapterPosition();
                List<N_city> n_cityList= list.get(id).n_cityList;
                Intent intent =new Intent(activity, ncityPickActivity.class);
                Bundle bundle=new Bundle();
                bundle.putSerializable("cityList", (Serializable) n_cityList);
                bundle.putString("province",name);
                intent.putExtras(bundle);
                activity.startActivity(intent);



            }
        });
        return holder;
    }




    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        City city=list.get(position);
        holder.p_name.setText(city.province);

    }


    @Override
    public int getItemCount() {
        return list!=null?list.size():0;
    }
}
