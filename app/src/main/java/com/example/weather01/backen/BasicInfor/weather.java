package com.example.weather01.backen.BasicInfor;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.weather01.backen.http.HttpGET;
import com.qweather.sdk.bean.geo.GeoBean;
import com.qweather.sdk.bean.weather.WeatherDailyBean;
import com.qweather.sdk.bean.weather.WeatherNowBean.NowBaseBean;

import org.json.JSONException;

import java.net.URLConnection;
public class weather{
    public String key="&key=bc3c26aacf6b45e28c5ef685dbf9835d";
    public String nbkey="&key=86a929366a674c589ff79b92b6681548";
    //得到七天的天气情况
    public JSONArray get7DbyAreaName(String AreaName) throws JSONException {
      JSONArray daliy7d=new JSONArray();
      JSONObject Daliy =new JSONObject();
      String latlon=new String();
      Location getlo=new Location();
      latlon= getlo.getLocation(AreaName).getString("lon")+","+getlo.getLocation(AreaName).getString("lat");
      HttpGET get = new HttpGET("https://api.qweather.com/v7/weather/7d?location="+latlon+nbkey);
      Daliy=get.re;
      if(!Daliy.getString("code").equals("200")){
          System.out.println("获取7天天气失败，请重新获取");
          return null;
      }else
      {
          daliy7d=Daliy.getJSONArray("daily");
//          JSONObject day1=daliy3d.getJSONObject(0);//第一天
//          day1.getString("fxDate");//得到日期的字符串，同理通过key得到其他的信息value
//          一个daliy中的json e.g:
          /*{
          "fxDate":"2021-06-10",
          "sunrise":"04:46",//日出
          "sunset":"19:43",//日落
          "moonrise":"04:25",//月生
          "moonset":"19:42",//月落
          "moonPhase":"新月",
          "tempMax":"30",
          "tempMin":"16",//预报当天最低温度
          "iconDay":"100",//预报白天天气状况的图标代码，图标可通过天气状况和图标下载
          "textDay":"晴",//预报白天天气状况的图标代码，图标可通过天气状况和图标下载
          "iconNight":"150",//预报夜间天气状况的图标代码，图标可通过天气状况和图标下载
          "textNight":"晴",//预报晚间天气状况文字描述，包括阴晴雨雪等天气状态的描述
          "wind360Day":"179",//预报白天风向360角度
          "windDirDay":"南风",//预报白天风向
          "windScaleDay":"1-2",//预报白天风力等级
          "windSpeedDay":"7",//预报白天风速，公里/小时
          "wind360Night":"0",//预报夜间风向360角度
          "windDirNight":"北风",//预报夜间当天风向
          "windScaleNight":"1-2",//预报夜间风力等级
          "windSpeedNight":"3",//预报夜间风速，公里/小时
          "humidity":"49",//预报当天总降水量，默认单位：毫米
          "precip":"0.0",//紫外线强度指数
          "pressure":"997",//相对湿度，百分比数值
          "vis":"25",//能见度，默认单位：公里
          "cloud":"14",//大气压强，默认单位：百帕
          "uvIndex":"11"//云量，百分比数值
          }*/

          return daliy7d;
//          daliy7d.getJSONObject(1);//第二天
//          daliy7d.getJSONObject(2);//第三天


      }

  }
   //得到一日的天气详情
    public JSONObject getNowWeather(String AreaName){
        JSONObject now= new JSONObject();
        Location getlo=new Location();
        String latlon=new String();
        latlon= getlo.getLocation(AreaName).getString("lon")+","+getlo.getLocation(AreaName).getString("lat");
        HttpGET get = new HttpGET("https://devapi.qweather.com/v7/weather/now?location="+latlon+key);
        if(!get.re.getString("code").equals("200")){
            System.out.println("无法获取当前天气详情");
            return null;
        }
        else {
            now = (JSONObject) get.re.get("now");
           /*
           * 获取举例：
           *   now.getString("feelslike")//获取当前体感温度
           * 参数说明：
           *   obsTime	数据观测时间
                temp	温度，默认单位：摄氏度
                feelsLike	体感温度，默认单位：摄氏度
                icon	天气状况和图标的代码，图标可通过天气状况和图标下载
                text	天气状况的文字描述，包括阴晴雨雪等天气状态的描述
                wind360	风向360角度
                windDir	风向
                windScale	风力等级
                windSpeed	风速，公里/小时
                humidity	相对湿度，百分比数值
                precip	当前小时累计降水量，默认单位：毫米
                pressure	大气压强，默认单位：百帕
                vis	能见度，默认单位：公里
                cloud	云量，百分比数值
                 dew	露点温度
           * */

            return now;
        }

    }

        //得到该地区的每小时天气情况
        public JSONArray getHourlybyAreaName(String AreaName) throws JSONException {
        JSONArray daliyHourly=new JSONArray();
        JSONObject Hour =new JSONObject();
        String latlon=new String();
        Location getlo=new Location();
        latlon= getlo.getLocation(AreaName).getString("lon")+","+getlo.getLocation(AreaName).getString("lat");
        HttpGET get = new HttpGET("https://api.qweather.com/v7/weather/24h?location="+latlon+nbkey);
        Hour=get.re;
        if(!Hour.getString("code").equals("200")){
            System.out.println("逐小时天气返回失败");
            return  null;
        }
        else{
        daliyHourly=Hour.getJSONArray("hourly");
        JSONObject day1=daliyHourly.getJSONObject(0);//得到第一个小时的具体数据
            System.out.println(day1.getString("text"));//取值举例 “晴”

        return daliyHourly;}
        /*
        * 返回实例
        * [
    {
      "fxTime": "2021-02-16T15:00+08:00",
      "temp": "2",
      "icon": "100",
      "text": "晴",
      "wind360": "335",
      "windDir": "西北风",
      "windScale": "3-4",
      "windSpeed": "20",
      "humidity": "11",
      "pop": "0",//降雨概率
      "precip": "0.0",
      "pressure": "1025",
      "cloud": "0",
      "dew": "-25"
    },
    {
      "fxTime": "2021-02-16T16:00+08:00",
      "temp": "1",
      "icon": "100",
      "text": "晴",
      "wind360": "339",
      "windDir": "西北风",
      "windScale": "3-4",
      "windSpeed": "24",
      "humidity": "11",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1025",
      "cloud": "0",
      "dew": "-26"
    },
    {
      "fxTime": "2021-02-16T17:00+08:00",
      "temp": "0",
      "icon": "100",
      "text": "晴",
      "wind360": "341",
      "windDir": "西北风",
      "windScale": "4-5",
      "windSpeed": "25",
      "humidity": "11",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1026",
      "cloud": "0",
      "dew": "-26"
    },
    {
      "fxTime": "2021-02-16T18:00+08:00",
      "temp": "0",
      "icon": "150",
      "text": "晴",
      "wind360": "344",
      "windDir": "西北风",
      "windScale": "4-5",
      "windSpeed": "25",
      "humidity": "12",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1025",
      "cloud": "0",
      "dew": "-27"
    },
    {
      "fxTime": "2021-02-16T19:00+08:00",
      "temp": "-2",
      "icon": "150",
      "text": "晴",
      "wind360": "349",
      "windDir": "西北风",
      "windScale": "3-4",
      "windSpeed": "24",
      "humidity": "13",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1025",
      "cloud": "0",
      "dew": "-27"
    },
    {
      "fxTime": "2021-02-16T20:00+08:00",
      "temp": "-3",
      "icon": "150",
      "text": "晴",
      "wind360": "353",
      "windDir": "北风",
      "windScale": "3-4",
      "windSpeed": "22",
      "humidity": "14",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1025",
      "cloud": "0",
      "dew": "-27"
    },
    {
      "fxTime": "2021-02-16T21:00+08:00",
      "temp": "-3",
      "icon": "150",
      "text": "晴",
      "wind360": "355",
      "windDir": "北风",
      "windScale": "3-4",
      "windSpeed": "20",
      "humidity": "14",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1026",
      "cloud": "0",
      "dew": "-27"
    },
    {
      "fxTime": "2021-02-16T22:00+08:00",
      "temp": "-4",
      "icon": "150",
      "text": "晴",
      "wind360": "356",
      "windDir": "北风",
      "windScale": "3-4",
      "windSpeed": "18",
      "humidity": "16",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1026",
      "cloud": "0",
      "dew": "-27"
    },
    {
      "fxTime": "2021-02-16T23:00+08:00",
      "temp": "-4",
      "icon": "150",
      "text": "晴",
      "wind360": "356",
      "windDir": "北风",
      "windScale": "3-4",
      "windSpeed": "18",
      "humidity": "16",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1026",
      "cloud": "0",
      "dew": "-27"
    },
    {
      "fxTime": "2021-02-17T00:00+08:00",
      "temp": "-4",
      "icon": "150",
      "text": "晴",
      "wind360": "354",
      "windDir": "北风",
      "windScale": "3-4",
      "windSpeed": "16",
      "humidity": "16",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1027",
      "cloud": "0",
      "dew": "-27"
    },
    {
      "fxTime": "2021-02-17T01:00+08:00",
      "temp": "-4",
      "icon": "150",
      "text": "晴",
      "wind360": "351",
      "windDir": "北风",
      "windScale": "3-4",
      "windSpeed": "16",
      "humidity": "16",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1028",
      "cloud": "0",
      "dew": "-27"
    },
    {
      "fxTime": "2021-02-17T02:00+08:00",
      "temp": "-4",
      "icon": "150",
      "text": "晴",
      "wind360": "350",
      "windDir": "北风",
      "windScale": "3-4",
      "windSpeed": "16",
      "humidity": "16",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1028",
      "cloud": "0",
      "dew": "-27"
    },
    {
      "fxTime": "2021-02-17T03:00+08:00",
      "temp": "-5",
      "icon": "150",
      "text": "晴",
      "wind360": "350",
      "windDir": "北风",
      "windScale": "3-4",
      "windSpeed": "16",
      "humidity": "16",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1028",
      "cloud": "0",
      "dew": "-27"
    },
    {
      "fxTime": "2021-02-17T04:00+08:00",
      "temp": "-5",
      "icon": "150",
      "text": "晴",
      "wind360": "351",
      "windDir": "北风",
      "windScale": "3-4",
      "windSpeed": "16",
      "humidity": "15",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1027",
      "cloud": "0",
      "dew": "-28"
    },
    {
      "fxTime": "2021-02-17T05:00+08:00",
      "temp": "-5",
      "icon": "150",
      "text": "晴",
      "wind360": "352",
      "windDir": "北风",
      "windScale": "3-4",
      "windSpeed": "16",
      "humidity": "14",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1026",
      "cloud": "0",
      "dew": "-29"
    },
    {
      "fxTime": "2021-02-17T06:00+08:00",
      "temp": "-5",
      "icon": "150",
      "text": "晴",
      "wind360": "355",
      "windDir": "北风",
      "windScale": "3-4",
      "windSpeed": "14",
      "humidity": "16",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1025",
      "cloud": "0",
      "dew": "-27"
    },
    {
      "fxTime": "2021-02-17T07:00+08:00",
      "temp": "-7",
      "icon": "150",
      "text": "晴",
      "wind360": "359",
      "windDir": "北风",
      "windScale": "3-4",
      "windSpeed": "16",
      "humidity": "20",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1024",
      "cloud": "0",
      "dew": "-26"
    },
    {
      "fxTime": "2021-02-17T08:00+08:00",
      "temp": "-5",
      "icon": "100",
      "text": "晴",
      "wind360": "1",
      "windDir": "北风",
      "windScale": "3-4",
      "windSpeed": "14",
      "humidity": "19",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1023",
      "cloud": "0",
      "dew": "-26"
    },
    {
      "fxTime": "2021-02-17T09:00+08:00",
      "temp": "-4",
      "icon": "100",
      "text": "晴",
      "wind360": "356",
      "windDir": "北风",
      "windScale": "3-4",
      "windSpeed": "14",
      "humidity": "17",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1023",
      "cloud": "0",
      "dew": "-25"
    },
    {
      "fxTime": "2021-02-17T10:00+08:00",
      "temp": "-1",
      "icon": "100",
      "text": "晴",
      "wind360": "344",
      "windDir": "西北风",
      "windScale": "3-4",
      "windSpeed": "14",
      "humidity": "14",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1024",
      "cloud": "0",
      "dew": "-26"
    },
    {
      "fxTime": "2021-02-17T11:00+08:00",
      "temp": "0",
      "icon": "100",
      "text": "晴",
      "wind360": "333",
      "windDir": "西北风",
      "windScale": "3-4",
      "windSpeed": "14",
      "humidity": "12",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1024",
      "cloud": "0",
      "dew": "-26"
    },
    {
      "fxTime": "2021-02-17T12:00+08:00",
      "temp": "1",
      "icon": "100",
      "text": "晴",
      "wind360": "325",
      "windDir": "西北风",
      "windScale": "3-4",
      "windSpeed": "14",
      "humidity": "10",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1025",
      "cloud": "16",
      "dew": "-28"
    },
    {
      "fxTime": "2021-02-17T13:00+08:00",
      "temp": "2",
      "icon": "100",
      "text": "晴",
      "wind360": "319",
      "windDir": "西北风",
      "windScale": "3-4",
      "windSpeed": "16",
      "humidity": "8",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1025",
      "cloud": "32",
      "dew": "-29"
    },
    {
      "fxTime": "2021-02-17T14:00+08:00",
      "temp": "2",
      "icon": "100",
      "text": "晴",
      "wind360": "313",
      "windDir": "西北风",
      "windScale": "3-4",
      "windSpeed": "16",
      "humidity": "9",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1025",
      "cloud": "48",
      "dew": "-27"
    }
  ],*/
    }

        /*
        * 返回实例
        * [
    {
      "fxTime": "2021-02-16T15:00+08:00",
      "temp": "2",
      "icon": "100",
      "text": "晴",
      "wind360": "335",
      "windDir": "西北风",
      "windScale": "3-4",
      "windSpeed": "20",
      "humidity": "11",
      "pop": "0",
      "precip": "0.0",
      "pressure": "1025",
      "cloud": "0",
      "dew": "-25"
    }*/

        //得到该地区的空气情况
        public JSONObject getAirConditionbyAreaName(String AreaName) {

            JSONObject Air =new JSONObject();
            String latlon=new String();
            Location getlo=new Location();
            latlon= getlo.getLocation(AreaName).getString("lon")+","+getlo.getLocation(AreaName).getString("lat");
            HttpGET get = new HttpGET("https://devapi.qweather.com/v7/air/now?location="+latlon+key);
            Air=get.re;
            if(!Air.getString("code").equals("200")){
                System.out.println("空气质量返回失败");
                return  null;
            }
            else{
                JSONObject Airnow=Air.getJSONObject("now");
                return Airnow;
            }
            /*
            * {
            * "pubTime":"2021-06-14T00:00+08:00",
            * "aqi":"33","level":"1",
            * "category":"优",
            * "primary":"NA",
            * "pm10":"17",
            * "pm2p5":"12",
            * "no2":"12",
            * "so2":"3",
            * "co":"0.6",
            * "o3":"103"
            * }*/
    }
        
  }
