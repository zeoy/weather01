package com.example.weather01.backen.Others;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

/**
 * 网络通信工具类
 * Created by SmileSB101 on 2016/11/1 0001.
 */

public class InternetUtill {
    /**
     * 网络请求队列
     */
    private static RequestQueue mRequestqueue;
    public static RequestQueue getmRequestqueue(Context context)
    {
        if(mRequestqueue == null)
        {
            mRequestqueue = Volley.newRequestQueue(context);
            return mRequestqueue;
        }
        else{
            return mRequestqueue;
        }
    }
}
