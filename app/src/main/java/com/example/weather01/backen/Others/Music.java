package com.example.weather01.backen.Others;

public class Music {
    private String name;
    private String url;
    private String id;
    private String author;
    private String pic;
    private String totalTime;

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getPic() {
        return pic;
    }

    public String getTotalTime() {
        return totalTime;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }
}
