package com.example.weather01.backen.BasicInfor;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.weather01.backen.http.HttpGET;



public class Indices {
    public String key="&key=bc3c26aacf6b45e28c5ef685dbf9835d";

    public static void sort(JSONArray unSortedArray){
        int x,y;
               for(int i=0;i<15;i++){
                         for(int j=0;j<15-i;j++){
                                 if(unSortedArray.getJSONObject(j).getIntValue("type")>unSortedArray.getJSONObject(j+1).getIntValue("type")){
                                         JSONObject temp = unSortedArray.getJSONObject(j);
                                         unSortedArray.set(j,unSortedArray.getJSONObject(j+1));
                                         unSortedArray.set(j+1,temp);
                                     }
                             }
                     }

            }


    public JSONArray getIndicesbyAreaName(String AreaName) {
        JSONArray Indices=new JSONArray();
        JSONObject indices=new JSONObject();
        String latlon=new String();
        Location getlo=new Location();
        latlon= getlo.getLocation(AreaName).getString("lon")+","+getlo.getLocation(AreaName).getString("lat");
        HttpGET get = new HttpGET("https://devapi.qweather.com/v7/indices/1d?type=1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16&location="+latlon+key);
        indices=get.re;
        if(!indices.getString("code").equals("200")){
            System.out.println("调取生活指数失败！");
            return null;
        }
        else {
        Indices= (JSONArray) indices.get("daily");


        /*
        例子：取洗车指数的内容（text）：
        Indices.getJSONObject(1),getString("text");
        返回的json例子如下：实际上列表中有16个指数，在下方有解释分别是什么
        "daily": [
    {
      "date": "2021-02-06",
      "type": "2",
      "name": "洗车指数",
      "level": "2",
      "category": "较适宜",
      "text": "较适宜洗车，未来一天无雨，风力较小，擦洗一新的汽车至少能保持一天。"
    },
    {
      "date": "2021-02-06",
      "type": "1",
      "name": "运动指数",
      "level": "3",
      "category": "较不宜",
      "text": "天气较好，但考虑天气寒冷，推荐您进行室内运动，户外运动时请注意保暖并做好准备活动。"
    }
  ],
        【0】运动指数		适宜(1)、较适宜(2)、较不宜(3)
        【1】洗车指数		适宜(1)、较适宜(2)、较不宜(3)、不宜(4)
        【2】穿衣指数		寒冷(1)、冷(2)、较冷(3)、较舒适(4)、舒适(5)、热(6)、炎热(7)
        【3】 钓鱼指数		适宜(1)、较适宜(2)、不宜(3)
        【4】紫外线指数		最弱(1)、弱(2)、中等(3)、强(4)、很强(5)
        【5】旅游指数		适宜(1)、较适宜(2)、一般(3)、较不宜(4)、不适宜(5)
        【6】花粉过敏指数		极不易发(1)、不易发(2)、较易发(3)、易发(4)、极易发(5)
        【7】舒适度指数		舒适(1)、较舒适(2)、较不舒适(3)、很不舒适(4)、极不舒适(5)、不舒适(6)、非常不舒适(7)
        【8】感冒指数		少发(1)、较易发(2)、易发(3)、极易发(4)
        【9】空气污染扩散条件指数		优(1)、良(2)、中(3)、较差(4)、很差(5)
        【10】空调开启指数		长时间开启(1)、部分时间开启(2)、较少开启(3)、开启制暖空调(4)
        【11】太阳镜指数		不需要(1)、需要(2)、必要(3)、很必要(4)、非常必要(5)
        【12】化妆指数		保湿(1)、保湿防晒(2)、去油防晒(3)、防脱水防晒(4)、去油(5)、防脱水(6)、防晒(7)、滋润保湿(8)
        【13】晾晒指数		极适宜(1)、适宜(2)、基本适宜(3)、不太适宜(4)、不宜(5)、不适宜(6)
        【14】交通指数		良好(1)、较好(2)、一般(3)、较差(4)、很差(5)
        【15】防晒指数		弱(1)、较弱(2)、中等(3)、强(4)、极强(5)*/
            sort(Indices);

        return Indices;

    }}
}
