package com.example.weather01.backen.Activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weather01.R;
import com.example.weather01.backen.BasicInfor.adapter.Area;
import com.example.weather01.backen.BasicInfor.adapter.AreaAdapter;

import java.util.List;

public class areaPickActivity extends AppCompatActivity {
  List<Area> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_pick);
        Bundle bundle=new Bundle();
        bundle=this.getIntent().getExtras();
        list= (List<Area>) bundle.getSerializable("cityList");
        String n_cityname=bundle.getString("n_cityname");
        AreaAdapter areaAdapter;
        areaAdapter=new AreaAdapter(getBaseContext(),list,this,n_cityname);
        RecyclerView recyclerView = findViewById(R.id.p_name);
        LinearLayoutManager managerSeven = new LinearLayoutManager(areaPickActivity.this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(managerSeven);
        recyclerView.setAdapter(areaAdapter);
    }
}