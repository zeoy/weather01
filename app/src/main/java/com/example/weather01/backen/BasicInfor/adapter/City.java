package com.example.weather01.backen.BasicInfor.adapter;


import java.util.List;

public class City {
    public String province;
    public List<N_city> n_cityList;
    public City(String province,List<N_city> n_cityList){
        this.province=province;
        this.n_cityList=n_cityList;
    }

    public List<N_city> getN_cityList() {
        return n_cityList;
    }

    public String getProvince(){
        return province;
    }

}

